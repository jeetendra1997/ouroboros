
extends Node

var entities = []
var currentUUID = 0
var database = {}

func createEntity():
	entities.append(currentUUID)
	entity = currentUUID
	currentUUID + 1
	return entity

func removeEntity(entity):
	for type in database.keys(): 
		database[type][entity].erase()

func addComponent(entity, component):
	if not (component.type() in database.keys()):
		database[component.type()] = {}
	database[component.type()][entity] = component

func removeComponent(entity, component):
	database[component.type()][entity].erase() 
	if database[component.type()] == {}:
		database[component.type()].erase()

func hasComponent(entity, component):
	return database[component.type()].has(entity)

func hasComponentType(entity, component):
	if component.type() in database.keys():
		if database[component.type()].has(entity):
			return true
	else:
		return false

func _ready():
	# Initialization here
	pass
	