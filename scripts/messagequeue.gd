extends Node

var subscribers = {};


func subscribe(eventName,object,method):
	if not subscribers.has(eventName):
		subscribers[eventName]=[]
	subscribers[eventName].push_back({"object":object,"method":method})

func unsubscribe(eventName,object):
	var index= 0 
	for sub in subscribers[eventName]:
		if object == sub["object"]:
			subscribers[eventName].remove(index)
		index=index+1

func emit(eventName,argument={}):
	for sub in subscribers[eventName]:
		sub["object"].call(sub["method"],argument)